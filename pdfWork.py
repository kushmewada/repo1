import PyPDF2 as Pdf


def file_information(pdf_path):
    with open(pdf_path, 'rb') as f:
        pdf = Pdf.PdfFileReader(f)
        info = pdf.getDocumentInfo()
        pages = pdf.getNumPages()
    txt = f'''
    Information about {pdf_path}
    
    Author: {info.author}
    Creator: {info.creator}
    Producer: {info.producer}
    Subject: {info.subject}
    Title: {info.title}
    Pages: {pages}
    '''

    print(txt)
    return info


def rotate_pdf(file_path, filename):
    pdf_reader = Pdf.PdfFileReader(f'{file_path}{filename}')
    pdf_writer = Pdf.PdfFileWriter()
    pdf_writer.addPage(pdf_reader.getPage(0))
    page_1 = pdf_reader.getPage(1).rotateClockwise(90)
    page_2 = pdf_reader.getPage(2).rotateClockwise(180)
    page_3 = pdf_reader.getPage(3).rotateCounterClockwise(90)
    pdf_writer.addPage(page_1)
    pdf_writer.addPage(page_2)
    pdf_writer.addPage(page_3)
    pdf_writer.addPage(pdf_reader.getPage(4))

    with open(f'{file_path}rotate.pdf', 'wb') as f:
        pdf_writer.write(f)


def merge_pdf(add_path, total_pdf, newfile):
    pdf_writer = Pdf.PdfFileWriter()
    for pdf in total_pdf:
        pdf_reader = Pdf.PdfFileReader(f'{add_path}{pdf}')
        for page in range(pdf_reader.getNumPages()):
            pdf_writer.addPage(pdf_reader.getPage(page))

    with open(f'{add_path}{newfile}', 'wb') as f:
        pdf_writer.write(f)


def split_pdf(file_path, pdf, split_file):
    pdf_reader = Pdf.PdfFileReader(f'{file_path}{pdf}')
    for i in range(pdf_reader.getNumPages()):
        pdf_writer = Pdf.PdfFileWriter()
        pdf_writer.addPage(pdf_reader.getPage(i))
        new_file_name = f"{file_path}{split_file}{i+1}.pdf"

        with open(f'{new_file_name}', 'wb') as f:
            pdf_writer.write(f)


if __name__ == '__main__':
    path = '/home/kush/Downloads/'
    file = 'python_tutorial.pdf'
    file2 = 'python_tutorial1.pdf'
    pdfs = [file, file2]
    newFile = 'merged.pdf'
    # file_information(path)
    # rotate_pdf(path, file)
    # merge_pdf(path, pdfs, "merged2.pdf")
    # split_pdf(path, 'rotate.pdf', 'split')

