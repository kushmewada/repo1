def decorator_maker_with_arguments(decorator_arg1, decorator_arg2, decorator_arg3):
    def decorator(func):
        def wrapper(function_arg1, function_arg2, function_arg3) :
            "This is the wrapper function"
            print("The wrapper can access all the variables\n"
                  "\t- from the decorator maker: {0} {1} {2}\n"
                  "\t- from the function call: {3} {4} {5}\n"
                  "and pass them to the decorated function"
                  .format(decorator_arg1, decorator_arg2,decorator_arg3,
                          function_arg1, function_arg2,function_arg3))
            return func(function_arg1, function_arg2,function_arg3)

        return wrapper

    return decorator

pandas = "Pandas"
@decorator_maker_with_arguments(pandas, "Numpy","Scikit-learn")
def decorated_function_with_arguments(function_arg1, function_arg2,function_arg3):
    print("This is the decorated function and it only knows about its arguments: {0}"
           " {1}" " {2}".format(function_arg1, function_arg2,function_arg3))

decorated_function_with_arguments(pandas, "Science", "Tools")



def mainDecorator(dArg, dArg2, dArg3):
    def deco(func):
        print('hello {0}, {1} and {2} from deco '.format(dArg, dArg2, dArg3))
        def wrapper(fArg, fArg2, fArg3):
            print('hello {0}, {1} and {2} from wrapper '.format(dArg, dArg2, dArg3))
            func(fArg, fArg2, fArg3)
        return wrapper
    return deco


@mainDecorator('user4', 'user5', 'user6')
def funcOutsidDecorator(arg1, arg2, arg3):
    print('hello {0}, {1} and {2} from funcOutsidDecorator'.format(arg1, arg2, arg3))

funcOutsidDecorator('user1', 'user2', 'user3')
