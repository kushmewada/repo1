import re
def fun(s):
    # return True if s is a valid email, else return False
    ptr = r"^[a-z0-9]+[\._-]*[a-z0-9]*[@][a-z0-9]+[.][a-z]{1,3}[.]?[a-z]{0,2}$"
    if re.match(ptr, s):
        return True
    else:
        return False
def filter_mail(emails):
    return list(filter(fun, emails))

if __name__ == '__main__':
    # n = int(input())
    emails = ['its@gmail.co.in', 'mike13445@yahoomail9.server', 'rase23@ha_ch.com', 'daniyal@gmail.coma', 'thatisit@thatisit']
    # for _ in range(n):
    #     emails.append(input())

    filtered_emails = filter_mail(emails)
    filtered_emails.sort()
    print(filtered_emails)