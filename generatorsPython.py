def fib(len1):
    a,b = 0, 1
    while a < len1:
        yield a
        a, b = b, a+b


if __name__ == "__main__":
    for i in fib(15):
        print(i)

    ls = [5,6,2,8,0,23,231]
    # newLs = numbr(ls)
    # print(list(newLs))
