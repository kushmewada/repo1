def getArgs(*name):
    for i in name:
        print(i,end=' ')
    print('')

getArgs('name1', 'name2', 'name3', 'name4')


def getKeyWordArgs(**name):
    for key, value in name.items():
        print('key =',key,"value =",value, end=' ')
    print('')

getKeyWordArgs(name='name1', email='email@domain.com', passw = "password")


def limitedArg(arg1, arg2, arg3):
    print(arg1)
    print(arg2)
    print(arg3)

arg = ['name', 'no', 12]
keyarg = {'arg1' : "Geeks", 'arg2' : 'for', 'arg3' : "Geeks"}

limitedArg(*arg)
limitedArg(**keyarg)

