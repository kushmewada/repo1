import math

class Complex(object):
    
    def __init__(self, real, imaginary):
        self.comp = (real, imaginary)

    def __add__(self, no):
        return Complex(self.comp + no.comp)
    
    def __sub__(self, no):
        return Complex(self.comp - no.comp)
    
    def __mul__(self, no):
        return Complex(self.comp * no.comp)
    
    def __truediv__(self, no):
        return Complex(self.comp / no.comp)
    
    def mod(self):
        return self.real % self.imaginary
    def __str__(self):
        if self.imaginary == 0:
            result = "%.2f+0.00i" % (self.real)
        elif self.real == 0:
            if self.imaginary >= 0:
                result = "0.00+%.2fi" % (self.imaginary)
            else:
                result = "0.00-%.2fi" % (abs(self.imaginary))
        elif self.imaginary > 0:
            result = "%.2f+%.2fi" % (self.real, self.imaginary)
        else:
            result = "%.2f-%.2fi" % (self.real, abs(self.imaginary))
        return result

if __name__ == '__main__':
    # c = map(float, input().split())
    # d = map(float, input().split())
    x = Complex(2, 1)
    y = Complex(5, 6)
    print(*map(str, [x+y, x-y, x*y, x/y, x.mod(), y.mod()]), sep='\n')