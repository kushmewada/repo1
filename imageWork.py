import imageio
import matplotlib.image as mpimg
import matplotlib.pyplot as py
from PIL import Image

# Read pdf
# fi = tabula.read_pdf('sample.pdf', pages='all')

# Read Image
img = imageio.imread('index.jpeg')
py.imshow(img)

# Convert pdf to csv
# # tabula.convert_into('sample.pdf', 'sample.csv', output_format='csv', pages='all')

# Convert image format
# imageio.imwrite('index.png', img)

# image handling with Pillow module
img = mpimg.imread('index.png')
with Image.open('sample.jpg') as image:
    width, height = image.size

    # Rotate image
    rImage = image.rotate(180.00)
    # rImage.save('sample.jpg')
    rotated = Image.open('sample.jpg')
    # rotated.show()

    # Crop image
    cropImage = image.crop((0, 0, int(width/2), int(width/2)))
    # cropImage.save('crop.jpg')
    # cropImage.show()

    # Resize image
    reimage = image.resize((500, 500))
    # reimage.show()
    # print(reimage.size)

    # Past one image in second image
    img2 = Image.open('sample2.jpg')
    img2 = img2.resize((5000, 3000))
    width2, height2 = img2.size
    image.paste(img2, (int((width-width2)/2), int((height-height2)/2)))  # margin from left and right
    # image.show()

    # Flip Image
    image = image.transpose(Image.FLIP_LEFT_RIGHT)
    # image.show()

    "Split image"
    nImage = image.split()
    # image.show()
    img2.close()

    # Thumbnail create
    image.thumbnail((2000, 2000))
    # print(image.size)
    # image.show()

# To bitmap
with Image.open('bm.jpg') as bnw:
    pass
    # print(bnw.mode)
    # print(bnw.tobitmap())


# open from different path
with Image.open('/home/kush/Downloads/pix.jpg') as bnw:
    bnw.show()
    bnw = bnw.transpose(Image.FLIP_LEFT_RIGHT)

    # Save file to different path
    # bnw.save('/home/kush/Downloads/newPix.jpg')

    # Mode of image
    # print(bnw.mode)

    # bitmap of image
    # print(bnw.tobitmap())


# plt.imshow(img)
# print(f"Image width = {width} and height = {height}")

