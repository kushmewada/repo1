import json
# with open('newText.txt', 'w') as f:
#     f.write('hello world\n')
#     f.write('This is 2nd line')

with open('newText.txt', 'r') as f:
    data = f.readlines()

# print(data[0])
data.insert(1, 'this is 3rd line insert in 2nd line\n')

# with open('newText.txt', 'w') as f:
#     f.writelines(data)

with open('newText.txt', 'r') as f:
    print(f.readlines())


data = {'1':'name', 'simple': 'user', 'list': 'user2'}
f = open('demo.json','a')
# json.dump(data, f, indent=4)
rf = open('demo.json','r')
print(json.load(rf))
f.close()
rf.close()

# with open('demo.json','rb+'):
#     json.dump(data, f)

