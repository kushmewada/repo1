import datetime
from functools import reduce
import re
from pytz import timezone
# m, d, y = map(int, input().split())
m, d, y = 8,15,2022

date = datetime.datetime(2015,5,10,13,54,36)
# print(date)
# timeZ = pytz.timezone('-700')
# new = timeZ.localize(date)
# print(timeZ.localize(date))
# print(new.strftime("%Z"))


# n = int(input())
n = 1
ans = 1
for i in range(n):
    date = [['Sun', '11', 'May', '2015', '13:54:36', '-0530'], ['Sun', '10', 'May', '2015', '13:54:36', '-0000']]
    # for i in range(2):
    #     date.append(input().split(maxsplit=6))
    # print(datetime.datetime.strptime(date[0][2], "%b").month)
    m = int(datetime.datetime.strptime(date[0][2], "%B").month)    
    m2 = int(datetime.datetime.strptime(date[1][2], "%B").month)

    findT = lambda x: re.split(":",x)
    t = findT(date[0][4])
    t2 = findT(date[1][4])
    dt1 = datetime.datetime(int(date[0][3]), m, int(date[0][1]), int(t[0]), int(t[1]), int(t[2]))
    dt2 = datetime.datetime(int(date[1][3]), m2, int(date[1][1]), int(t2[0]), int(t2[1]), int(t2[2]))
    d3 = dt1-dt2


    # print(d3)


    getD = re.split(r'[,]',str(d3))
    if len(getD) > 1:
        x = "".join([x for x in getD[0] if x.isdigit()])
        y = getD[1].split(r":")
        ans = (int(x)*24*3600) + (int(y[0])*3600) + (int(y[1])*60) + int(y[2])
        print(ans)
    else:
        y = getD[0].split(r":")
        ans = (int(y[0])*3600) + (int(y[1])*60) + int(y[2])

    difHour = []
    for i in range(2):
        temp = "".join([x for x in date[i][5] if x.isdigit()])
        difHour.append(int(temp))
    difHAns = reduce(lambda x,y: x-y, difHour)
    h = (str(difHAns)[:-2])
    m = (str(difHAns)[-2:])
    ans += (int(h)*3600) + (int(m)*60)
    print(ans)
