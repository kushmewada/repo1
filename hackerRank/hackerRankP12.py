# Sample Input
# 5
# 1 2 3 6 5 4 4 2 5 3 6 1 6 5 3 2 4 1 2 5 1 4 3 6 8 4 3 1 5 6 2 || 8 is k's room
# Sample Output
# 8


# Enter your code here. Read input from STDIN. Print output to STDOUT
N = int(input())
ls = [int(x) for x in input().split()]
temp = []
find = False
ls.sort(reverse=True)
t100 = int(len(ls))
t50 = int(t100 / 2)
t25 = int(t50 / 2)
t75 = int(t50 + t25)

for i in range(0, t25, 10):
    c = ls.count(ls[i])
    if c != N:
        print(ls[i])
        find = True
        break
    if ls[i] in temp:
        continue
    temp.append(ls[i])

if not find:
    for i in range(t50, t75, 10):
        c = ls.count(ls[i])
        if c != N:
            print(ls[i])
            find = True
            break
        if ls[i] in temp:
            continue
        temp.append(ls[i])

if not find:
    for i in range(0, t25, 10):
        c = ls.count(ls[i])
        if c != N:
            print(ls[i])
            find = True
            break
        if ls[i] in temp:
            continue
        temp.append(ls[i])

if not find:
    for i in range(t25, t50, 10):
        c = ls.count(ls[i])
        if c != N:
            print(ls[i])
            find = True
            break
        if ls[i] in temp:
            continue
        temp.append(ls[i])