import re


def count_substring(string, sub_string):
    char = sub_string[0]
    ls = re.findall(r"[A-Z][^A-Z]*", string)
    n = 0
    temp = False
    # return ls
    for i in range(len(ls)):
        if len(ls[i]) > 0:
            for j in range(len(ls[i])):
                if ls[i][j] == char:
                    n = i
                    temp = True
                    break
        else:
            if ls[i] == char:
                n = i
                break
        if temp:
            break
    return n


if __name__ == '__main__':
    # string = input().strip()
    string = "ThIsisCoNfUsInG"
    # sub_string = input().strip()
    sub_string = "is"

    count = count_substring(string, sub_string)
    print(count)
    # st = "AaaBabCac"
    # ls = re.findall(r"[A-Z][^A-Z]*", st)
    # print(ls)