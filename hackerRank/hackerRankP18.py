import textwrap


def wrap(string, max_width):
    wrap = textwrap.TextWrapper(width=max_width)
    wrap_text = wrap.wrap(text=string)
    return '\n'.join(x for x in wrap_text)


if __name__ == '__main__':
    # string, max_width = input(), int(input())
    string = "bscnksbcjscksbcjksbckjdscsbdcbsdkjbcsdjcbsdjkcbsdkjbckjdsbjksd"
    max_width = 9
    result = wrap(string, max_width)
    print(result)
