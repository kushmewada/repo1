"""
#size 3         #size 5 
                --------e--------
----c----       ------e-d-e------
--c-b-c--       ----e-d-c-d-e----
c-b-a-b-c       --e-d-c-b-c-d-e--
--c-b-c--       e-d-c-b-a-b-c-d-e
----c----       --e-d-c-b-c-d-e--
                ----e-d-c-d-e----
                ------e-d-e------
                --------e--------

"""
import string

n = 10 # input

def ptrn(n,alp):
    ls = []
    alpha = 0
    for i in range(n):
        if i > int(n/2):
            alpha -= 1
            ls.append(str(string.ascii_lowercase[alp-(alpha)]))
        else:
            ls.append(str(string.ascii_lowercase[alp-(alpha+1)]))
            alpha += 1
    name = "".join([ls[x]+"-" if x!=len(ls)-1 else ls[x] for x in range(len(ls))])
    return name

h = n + (n-1)
w = h + (h-1)
ptrnL = 1
count = 1

for i in range(1,n+1):
    li = ''.join([str(string.ascii_lowercase[n-(i):n]) for x in range(1,i+1)])
    p = ptrn(count,n)
    lent = len(p)
    print(p.center(w,"-"))
    count += 2
count -= 2

for i in range(1,n):
    count -= 2
    li = ''.join([str(string.ascii_lowercase[n-(i):n]) for x in range(1,i+1)])
    p = ptrn(count,n)
    lent = len(p)
    print(p.center(w,"-"))