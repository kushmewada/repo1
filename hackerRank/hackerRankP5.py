# Enter your code here. Read input from STDIN. Print output to STDOUT
from collections import defaultdict

dic = defaultdict(list)
ls = list(map(int, input().split()))
n = ls[0]
m = ls[1]
groupA = []
groupB = []
# groupA = list(map(str, input().split()))
for i in range(n):
    groupA.append(input())
for i in range(m):
    groupB.append(input())

for j in groupB:
    if j not in groupA:
        print(-1, end=" ")
    else:
        for i in range(n):
            if j == groupA[i]:
                print(i + 1, end=" ")
    print('')

