# print decimal, octal, Hexadecimal and binary number beteen 1 to n in format 
""" Ex:
    1     1     1     1
    2     2     2    10
    3     3     3    11
    4     4     4   100
    5     5     5   101
    6     6     6   110
"""

number = 17

tripNum = lambda x: ''.join([x[i] for i in range(len(x)) if i>1])
size = len(tripNum(list(bin(number))))

for i in range(1,number+1):
    print(str(i).rjust(5), end=" ")
    print(tripNum(list(oct(i))).rjust(5), end=" ")
    print(tripNum(list(hex(i))).rjust(5).upper(), end=" ")
    print(tripNum(list(bin(i))).rjust(5))

