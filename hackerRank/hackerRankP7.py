n = int(input())
dic = {}
for i in range(n):
    data = input()
    name = "".join([x for x in data if x.isalpha() or x == " "])
    price = int("".join([x for x in data if x.isdigit()]))
    data = {name: price}
    if name in dic:
        dic[name] += price
    else:
        dic.update({name: price})

for name in dic:
    print("{}{}".format(name, dic[name]))
    