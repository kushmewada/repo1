import re

if __name__ == '__main__':
    # N = int(input())
    dic = {}
    ls = []
    Run = True
    while Run:
        data = input()
        temp = 0
        # inpN = int(len(st))
        getIn1 = re.findall("[a-zA-Z]+", data)
        if not getIn1:
            print("Command - Help\n\tinsert\n\tappend\n\tprint\n\tremove\n\tpop\n\tsort\n\treverse")
            break
        comm = getIn1[0]
        if comm == 'insert':
            get = re.findall('[0-9]+[0-9]*', data)
            ind = int(get[0])
            val = int(get[1])
            ls.insert(ind, val)
        elif comm == 'remove':
            get = re.findall('[0-9]+', data)
            val = int(get[0])
            ls.remove(val)
        elif comm == 'pop':
            ls.pop()

        elif comm == 'print':
            print(ls)

        elif comm == 'append':
            get = re.findall('[0-9]+', data)
            val = int(get[0])
            ls.append(val)

        elif comm == 'sort':
            ls.sort()

        elif comm == 'reverse':
            ls.reverse()
        else:
            print("Command - Help\n\tinsert\n\tappend\n\tprint\n\tremove\n\tpop\n\tsort\n\treverse")
            break
