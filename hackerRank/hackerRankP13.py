def split_and_join(line):
    ls = line.split(" ")
    return "-".join([x for x in ls])


if __name__ == '__main__':
    line = input()
    result = split_and_join(line)
    print(result)