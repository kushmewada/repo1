"""
Kevin and Stuart want to play the 'The Minion Game'.

Game Rules

Both players are given the same string, .
Both players have to make substrings using the letters of the string .
Stuart has to make words starting with consonants.
Kevin has to make words starting with vowels.
The game ends when both players have made all possible substrings.

Scoring
A player gets +1 point for each occurrence of the substring in the string .

For Example:
String  = BANANA
Kevin's vowel beginning word = ANA
Here, ANA occurs twice in BANANA. Hence, Kevin will get 2 Points.
"""

def minion_game(string):
    stuart = 0
    kevin = 0
    string.upper()
    vowels = list('AEIOU')
    mainLi = []

    for x in string:
        if x not in mainLi:
            c = list(string).count(x)
            if x not in vowels:
                stuart += (1*c)
            else:
                kevin += (1*c)
            mainLi.append(x)
    
    ls = list(string)

    for i in range(len(ls)-1):
        temp = "".join([ls[x] for x in range(len(ls)) if x >= i])
        for j in range(len(temp)-1):
            temp2 = "".join([list(temp)[x] for x in range(len(temp)) if x <= (len(temp)-(j+1))])
            if len(temp2) > 0:
                if temp2[0] not in vowels:
                    stuart += 1
                else:
                    kevin += 1
        
    if stuart > kevin:
        print("Stuart", stuart)
    elif kevin > stuart:
        print("Kevin", kevin)
    else:
        print("Draw")


if __name__ == '__main__':
    # s = input()
    s = "BAANANAS"
    minion_game(s)