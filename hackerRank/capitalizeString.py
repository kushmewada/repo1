def solve(s):
    ls = (s.split(' '))
    return " ".join([x.capitalize() for x in ls])

if __name__ == '__main__':
    # fptr = open(os.environ['OUTPUT_PATH'], 'w')

    # s = input()
    s = "hello 99moto"
    result = solve(s)
    print(result)

    # fptr.write(result + '\n')
    # fptr.close()