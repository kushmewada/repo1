# Enter your code here. Read input from STDIN. Print output to STDOUT
# hacker rank logo design

# arrow = 5

from itertools import count


def hLine(n):
    if n == 0:
        return n
    count = 0
    for i in range(1,n+1):
        if i%2 != 0:
            count += 1
    return count


n = int(input())
m = n + (n - 1)
n1 = 1
char = 'H'
total = (m * 3) + 2




for i in range(1, n + 1):
    new = char * n1
    print(new.center(m))
    n1 += 2

hl = n + 1

for i in range(hl):
    new = char * n
    spa = " "
    sCount = spa * (n * 3)
    st = new + sCount + new
    print(st.center(total))

for i in range(hLine(n)):
    new = char * (total - (n-1))
    print(new.center(total))

for i in range(hl):
    new = char * n
    spa = " "
    sCount = spa * (n*3)
    st = new+sCount+new
    print(st.center(total))

for i in range(n):
    n1 -= 2
    new = char * n1
    print(new.rjust(total - i))