# Enter your code here. Read input from STDIN. Print output to STDOUT
from collections import namedtuple
n = int(input())
colunm = list(map(str, input().split()))
Data = namedtuple('Data','{}, {}, {}, {}'.format(colunm[0], colunm[1], colunm[2], colunm[3]))
ls = {}
all_marks = []
for i in range(n):
    name = "data{}".format(i+1)
    li = list(input().split())
    data = Data(li[0], li[1], li[2], li[3])
    ls.update({name: data})
for i in range(n):
    name = "data{}".format(i+1)
    all_marks.append(int(ls[name].MARKS))
avg = sum(all_marks)/n
print('%.2f' % avg)
