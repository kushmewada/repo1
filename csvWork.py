import csv
import pandas

# Read CSV file with csv module
with open('/home/kush/Downloads/sample.csv') as f:
    csvReader = csv.reader(f)
    header = next(csvReader)
    data = []
    for row in csvReader:
        data.append(row)
    # print(header)
    # print(data)

with open('/home/kush/Downloads/sample.csv') as f:
    csvReader = f.readlines()
header = csvReader[:1]
data = csvReader[1:]
# print(header)
# print(data)

# Read csv file with Pandas
dataP = pandas.read_csv('/home/kush/Downloads/sample.csv')
# print(dataP)
# print(list(dataP.columns))
# print(f"Experience\n{dataP.YearExperience}")
# print(f"Salary\n{dataP.Salary}")

# Write CSV file
headerW = ['Name', 'M1Score', 'M2Score']
dataW = [['Chirag', 40, 56], ['Parth', 35, 48], ['Kush', 38, 45]]
file = '/home/kush/Downloads/write.csv'
# with open(file, 'w') as f:
#     for i in range(len(headerW)):
#         if i+1 == len(headerW):
#             f.write(f"{str(headerW[i])}")
#         else:
#             f.write(f"{str(headerW[i])},")
#     f.write('\n')
#     for i in range(len(dataW)):
#         for j in range(len(dataW[i])):
#             if j + 1 == len(dataW[i]):
#                 f.write(f"{str(dataW[i][j])}")
#             else:
#                 f.write(f"{str(dataW[i][j])},")
#         f.write('\n')

# Write CSV file with pandas
pandasWrite = pandas.DataFrame(dataW, columns=headerW)
pandasWrite.to_csv(file, index=False)


# Read recently write file
readPandas = pandas.read_csv(file)
print(list(readPandas.columns))
# print(readPandas.M2Score)

