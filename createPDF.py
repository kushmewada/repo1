# Create new pdf from images

import PyPDF2
from PIL import Image
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import mm
from io import BytesIO

def make_pdf(name, img):
    pdf_writer = PyPDF2.PdfFileWriter()
    for i in range(len(img)):
        # open image file
        img1 = Image.open(img[i])
        width, height = img1.size
        newH = 0
        newW = 0
        imageTemp = BytesIO()
        imageDoc = canvas.Canvas(imageTemp, pagesize=A4)   #A4 = (210*mm,297*mm)

        # set heigt or width form perfect fit and get margin of x , y for set in middle
        if height > width:
            newH = 297*mm
            newW = (width*newH)/height
            y = 0
            x = (210*mm - newW)/2
        else:
            newW = 210*mm
            newH = (height*newW)/width
            y = ((297*mm) - newH)/2
            x = 0
        
        imageDoc.drawImage(img[i], x, y, width=newW, height=newH)
        imageDoc.save()
        page = PyPDF2.PdfFileReader(BytesIO(imageTemp.getvalue())).getPage(0)
        # image1 = Image.open(img[i])
        pdf_writer.addPage(page)
        # image1.close()

    # write in new pdf file
    with open(name,'wb') as f:
        pdf_writer.write(f)



if __name__ == '__main__':
    # newFile = input()
    # img = input().split()
    newFile = '/home/kush/Desktop/git/bitbucket/program/res/testPDF.pdf'
    img = ['/home/kush/Desktop/git/bitbucket/program/res/index.png', '/home/kush/Desktop/git/bitbucket/program/res/sample2.jpg']
    make_pdf(newFile, img)
