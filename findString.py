# find sub string repeat time from parent string

import re
import math

def count_substring(string, sub_string):
    count = 0
    m = string.find(sub_string)
    for i in range(math.ceil(len(string)/len(sub_string))):
        m = string.find(sub_string)
        if m == -1:
            return count
        count += 1
        ls = list(string)
        ls.pop(m)
        string = "".join([x for x in ls])
    
    return count


if __name__ == '__main__':
    # string = input().strip()
    # sub_string = input().strip()
    string = "I am an Indian, by birth."
    sub_string = 'Birth'
    
    count = count_substring(string, sub_string)
    print(count)
