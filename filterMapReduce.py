import re
from functools import reduce
# def func1(arg):
#     return arg

ls = ['arg1', 'arg2', 'arg3']

# for i in ls:
#     print(func1(i))

mapls = map(lambda x: True if(re.match('[a-z]+[1]', x)) else False, ls)
# print(list(mapls))


def start_with_A(li):
    return li[0] == 'A'

ls2 = [50, 30, 14, 11, 10, 9, 8, 7, 6]

fruit = ["Apple", "Banana", "Pear", "Apricot", "Orange"]
filter_object = filter(lambda x: x==14, ls2)
print(list(filter_object))

# print(reduce(lambda a,b: a+b, ls2))

def sumFun(args):
    sum1 = 0
    for i in args:
        sum1 += i
        # return args[i]
    return sum1

# print(sumFun(ls2))

